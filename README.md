# Asus T100-TA Magic Stick (Mirror)

[![][xda-badge]][xda-ref]
[![][bintray-badge]][bintray-ref]
[![][unlicense-badge]](UNLICENSE)

[bintray-badge]: https://img.shields.io/bintray/v/oxr463/linux-asus-t100ta/Asus_T100-TA_Magic_Stick.svg?style=flat-square
[bintray-ref]: https://bintray.com/oxr463/linux-asus-t100ta/Asus_T100-TA_Magic_Stick
[unlicense-badge]: http://img.shields.io/badge/license-unlicense-black.svg?style=flat-square
[xda-badge]: https://img.shields.io/badge/homepage-xda--developers-%23f59812.svg?style=flat-square
[xda-ref]: https://forum.xda-developers.com/windows-8-rt/win-8-development/live-asus-t100-ta-magic-stick-t3091481