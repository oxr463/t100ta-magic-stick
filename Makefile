SRCDIR = .
OUTDIR = "${SRCDIR}/tmp"
DISTDIR = "${SRCDIR}/dist"

DISTNAME = "T100TA Magic Stick 2.0-beta1.zip"
OUTNAME = "t100ta-magic-stick-2.0_beta_rc1.tar.xz"

DISTFILE = "${DISTDIR}/${DISTNAME}"
OUTFILE = "${OUTDIR}/${OUTNAME}"

MAXSIZE = "250M"

.PHONY: dist

dist: clean
	@mkdir -p "${OUTDIR}"
	@unzip "${DISTFILE}" -d "${OUTDIR}"
	@cd "${OUTDIR}" && tar -cfJ "${OUTFILE}" * .disk
	@split -b "${MAXSIZE}" "${OUTFILE}" "${OUTFILE}.part"

clean:
	@rm -rf "${OUTDIR}"
